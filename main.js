/**
 * Created by Daren on 7/4/16.
 */

var express = require("express");

var app = express();

app.use( 
    express.static(__dirname + "/public") 
);

app.listen(3000, function(){ 
    console.info("Application has started / is listening on port 3000"); 
});